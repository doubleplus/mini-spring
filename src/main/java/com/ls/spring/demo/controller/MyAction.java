package com.ls.spring.demo.controller;

import com.ls.spring.demo.service.IModifyService;
import com.ls.spring.demo.service.IQueryService;
import com.ls.spring.framework.annotation.MyAutowired;
import com.ls.spring.framework.annotation.MyController;
import com.ls.spring.framework.annotation.MyRequestMapping;
import com.ls.spring.framework.annotation.MyRequestParam;
import com.ls.spring.framework.webmvc.servlet.MyModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 公布接口url
 */
@MyController
@MyRequestMapping("/web")
public class MyAction {

    @MyAutowired
    IQueryService queryService;
    @MyAutowired
    IModifyService modifyService;

    @MyRequestMapping("/query.json")
    public MyModelAndView query(HttpServletRequest request, HttpServletResponse response,
                                @MyRequestParam("name") String name) {
        String result = queryService.query(name);
        return out(response, result);
    }

    @MyRequestMapping("/add*.json")
    public MyModelAndView add(HttpServletRequest request, HttpServletResponse response,
                              @MyRequestParam("name") String name, @MyRequestParam("addr") String addr) {
        String result = modifyService.add(name, addr);
        return out(response, result);
    }

    @MyRequestMapping("/remove.json")
    public MyModelAndView remove(HttpServletRequest request, HttpServletResponse response,
                                 @MyRequestParam("id") Integer id) {
        String result = modifyService.remove(id);
        return out(response, result);
    }

    @MyRequestMapping("/edit.json")
    public MyModelAndView edit(HttpServletRequest request, HttpServletResponse response,
                               @MyRequestParam("id") Integer id,
                               @MyRequestParam("name") String name) {
        String result = modifyService.edit(id, name);
        return out(response, result);
    }


    private MyModelAndView out(HttpServletResponse resp, String str) {
        try {
            resp.getWriter().write(str);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
