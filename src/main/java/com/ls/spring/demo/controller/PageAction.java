package com.ls.spring.demo.controller;

import com.ls.spring.demo.service.IQueryService;
import com.ls.spring.framework.annotation.MyAutowired;
import com.ls.spring.framework.annotation.MyController;
import com.ls.spring.framework.annotation.MyRequestMapping;
import com.ls.spring.framework.annotation.MyRequestParam;
import com.ls.spring.framework.webmvc.servlet.MyModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * 公布接口url
 */
@MyController
@MyRequestMapping("/")
public class PageAction {

    @MyAutowired
    IQueryService queryService;

    @MyRequestMapping("/first.html")
    public MyModelAndView query(@MyRequestParam("teacher") String teacher) {
        String result = queryService.query(teacher);
        Map<String, Object> model = new HashMap<>();
        model.put("teacher", teacher);
        model.put("data", result);
        model.put("token", "123456");
        return new MyModelAndView("first.html", model);
    }

}
