package com.ls.spring.framework.webmvc.servlet;

import lombok.Getter;

import java.util.Map;

/**
 * @author 挥之以墨
 */
@Getter
public class MyModelAndView {
    private String viewName;
    private Map<String, ?> model;

    public MyModelAndView(String viewName, Map<String, ?> model) {
        this.viewName = viewName;
        this.model = model;
    }

    public MyModelAndView(String viewName) {
        this.viewName = viewName;
    }
}
