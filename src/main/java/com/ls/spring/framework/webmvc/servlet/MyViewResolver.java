package com.ls.spring.framework.webmvc.servlet;

import org.apache.commons.lang3.StringUtils;

import java.io.File;

/**
 * @author 挥之以墨
 */
public class MyViewResolver {
    private final static String DEFAULT_TEMPLATE_SUFFIX = ".html";
    private File templateRootDir;

    public MyViewResolver(File templateRootDir) {
        this.templateRootDir = templateRootDir;
    }

    public MyView resolveViewName(String viewName) {
        if (StringUtils.isBlank(viewName)) {
            return null;
        }
        viewName = viewName.endsWith(DEFAULT_TEMPLATE_SUFFIX) ? viewName : (viewName + DEFAULT_TEMPLATE_SUFFIX);
        return new MyView(new File((templateRootDir.getPath() + "/" + viewName).replaceAll("/+", "/")));
    }

}
