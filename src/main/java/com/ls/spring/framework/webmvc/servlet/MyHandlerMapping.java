package com.ls.spring.framework.webmvc.servlet;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.lang.reflect.Method;
import java.util.regex.Pattern;

/**
 * 处理器的映射器
 * 通过URL找到需要执行的handler，也就是Controller
 *
 * @author 挥之以墨
 */
@Data
@AllArgsConstructor
public class MyHandlerMapping {
    /**
     * url
     */
    private Pattern pattern;

    /**
     * 对应的方法
     */
    private Method method;

    /**
     * method对应的对象
     */
    private Object controller;

}
