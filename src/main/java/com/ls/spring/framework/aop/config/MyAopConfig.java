package com.ls.spring.framework.aop.config;

import lombok.Data;

/**
 * @author 挥之以墨
 */
@Data
public class MyAopConfig {
    private String pointCut;
    private String aspectClass;
    private String aspectBefore;
    private String aspectAfter;
    private String aspectAfterThrow;
    private String aspectAfterThrowingName;
}
