package com.ls.spring.framework.beans.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * bean定义类，保存beanName和bean的完全限定名
 *
 * @author 挥之以墨
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MyBeanDefinition {
    /**
     * 首字母小写的beanName
     */
    private String factoryBeanName;
    /**
     * bean全类名
     */
    private String beanClassName;
}