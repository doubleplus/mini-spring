package com.ls.spring.framework.beans;

import lombok.Data;

/**
 * @author 挥之以墨
 */
@Data
public class MyBeanWrapper {
    /**
     * bean对象
     */
    private Object wrapperInstance;
    /**
     * bean的Class对象，备用
     */
    private Class<?> beanClass;

    public MyBeanWrapper(Object instance) {
        this.wrapperInstance = instance;
        this.beanClass = instance.getClass();
    }

}