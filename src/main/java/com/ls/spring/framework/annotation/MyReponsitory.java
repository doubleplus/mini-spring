package com.ls.spring.framework.annotation;

import java.lang.annotation.*;

/**
 * @author 挥之以墨
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@MyComponent
public @interface MyReponsitory {
    String value() default "";
}
