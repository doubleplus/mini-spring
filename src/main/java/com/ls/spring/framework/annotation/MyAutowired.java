package com.ls.spring.framework.annotation;

import java.lang.annotation.*;

/**
 * @author 挥之以墨
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyAutowired {
    String value() default "";
}
