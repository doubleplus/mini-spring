package com.ls.spring.framework.annotation;

import java.lang.annotation.*;

/**
 * @author 挥之以墨
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyRequestMapping {
    String value() default "";
}
